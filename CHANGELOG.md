## [1.1.2] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

# [1.1.1] - 2019-06-24
### Fixed
- API calls now respect the delay between calls

# [1.1.0] - 2019-06-14
### Added
- Analysis report now provide normalized note

# [1.0.4] - 2019-06-05
### Added
- Analysis report now provide the analysis date

## [1.0.2] - 2019-06-03
### Removed
- config.json has been removed, because it could leads to some build errors

## [1.0.1] - 2019-04-18
### Added
- NPM publish as GitLab CD

### Changed
- Upgrade `@fabernovel/heart-core`to latest version (the one that embed module loading code)
- Updated code according to the upgrade
- Updated the readme and contributing guide
- Improved the GitLab CI/CD

### Fixed
- NPM permissions from the GitLab CI/CD

## [1.0.0] - 2019-04-10
### Added
- First release (Yay!)
